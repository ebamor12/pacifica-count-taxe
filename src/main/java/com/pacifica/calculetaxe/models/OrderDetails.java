package com.pacifica.calculetaxe.models;

import java.util.Objects;

public class OrderDetails {

    private Integer quantity;
    private Product product;
    private String importLibelle;

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public String getImportLibelle() {
        return importLibelle;
    }

    public void setImportLibelle(String importLibelle) {
        this.importLibelle = importLibelle;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "OrderDetails{" +
                "quantity=" + quantity +
                ", product=" + product +
                ", importLibelle='" + importLibelle + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderDetails)) return false;
        OrderDetails that = (OrderDetails) o;
        return Objects.equals(getQuantity(), that.getQuantity()) &&
                Objects.equals(getProduct(), that.getProduct()) &&
                Objects.equals(getImportLibelle(), that.getImportLibelle());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getQuantity(), getProduct(), getImportLibelle());
    }
}
