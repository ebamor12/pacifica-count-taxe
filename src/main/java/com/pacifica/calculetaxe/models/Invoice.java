package com.pacifica.calculetaxe.models;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

public class Invoice {

    private List<InvoiceDetails> invoiceDetails;
    private BigDecimal totalProductsTaxe;
    private BigDecimal totaleProductsPrices;

    public List<InvoiceDetails> getInvoiceDetails() {
        return invoiceDetails;
    }

    public void setInvoiceDetails(List<InvoiceDetails> invoiceDetails) {
        this.invoiceDetails = invoiceDetails;
    }

    public BigDecimal getTotalProductsTaxe() {
        return totalProductsTaxe;
    }

    public void setTotalProductsTaxe(BigDecimal totalProductsTaxe) {
        this.totalProductsTaxe = totalProductsTaxe;
    }

    public BigDecimal getTotaleProductsPrices() {
        return totaleProductsPrices;
    }

    public void setTotaleProductsPrices(BigDecimal totaleProductsPrices) {
        this.totaleProductsPrices = totaleProductsPrices;
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "invoiceDetails=" + invoiceDetails +
                ", totalProductsTaxe=" + totalProductsTaxe +
                ", totaleProductsPrices=" + totaleProductsPrices +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Invoice)) return false;
        Invoice invoice = (Invoice) o;
        return Objects.equals(getInvoiceDetails(), invoice.getInvoiceDetails()) &&
                Objects.equals(getTotalProductsTaxe(), invoice.getTotalProductsTaxe()) &&
                Objects.equals(getTotaleProductsPrices(), invoice.getTotaleProductsPrices());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getInvoiceDetails(), getTotalProductsTaxe(), getTotaleProductsPrices());
    }
}
