package com.pacifica.calculetaxe.models;

import com.pacifica.calculetaxe.common.enumerations.ProductTypeEnum;

import java.math.BigDecimal;
import java.util.Objects;

public class Product {

    private String libelle;
    private BigDecimal unitPriceWithoutTaxe;
    private boolean isImported;
    private ProductTypeEnum productType;

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public BigDecimal getUnitPriceWithoutTaxe() {
        return unitPriceWithoutTaxe;
    }

    public void setUnitPriceWithoutTaxe(BigDecimal unitPriceWithoutTaxe) {
        this.unitPriceWithoutTaxe = unitPriceWithoutTaxe;
    }

    public boolean isImported() {
        return isImported;
    }

    public void setImported(boolean imported) {
        isImported = imported;
    }

    public ProductTypeEnum getProductType() {
        return productType;
    }

    public void setProductType(ProductTypeEnum productType) {
        this.productType = productType;
    }

    @Override
    public String toString() {
        return "Product{" +
                "libelle='" + libelle + '\'' +
                ", unitPriceWithoutTaxe=" + unitPriceWithoutTaxe +
                ", isImported=" + isImported +
                ", productType=" + productType +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return isImported() == product.isImported() &&
                Objects.equals(getLibelle(), product.getLibelle()) &&
                Objects.equals(getUnitPriceWithoutTaxe(), product.getUnitPriceWithoutTaxe()) &&
                getProductType() == product.getProductType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLibelle(), getUnitPriceWithoutTaxe(), isImported(), getProductType());
    }
}
