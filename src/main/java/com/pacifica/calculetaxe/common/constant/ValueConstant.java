package com.pacifica.calculetaxe.common.constant;

import java.math.BigDecimal;

public final class ValueConstant {

    public static final Integer ZERO_PERCENT                                 = 0;
    public static final Integer TEN_PERCENT                                  = 10;
    public static final Integer TWENTY_PERCENT                               = 20;
    public static final Integer TWO                                          = 2;
    public static final BigDecimal FIVE_CENT                                 = BigDecimal.valueOf(0.05);
    public static final Integer HUNDRED                                      = 100;
    public static final Integer FIVE_PERCENT                                 = 5;
    
    private ValueConstant() {

    }
}