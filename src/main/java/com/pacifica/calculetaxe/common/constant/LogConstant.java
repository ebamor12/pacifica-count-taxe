package com.pacifica.calculetaxe.common.constant;

public final class LogConstant {

    public static final String CREATE_ORDER_LOG                                ="Create order from file: ";
    public static final String CREATE_INVOICE_FILE_LOG                         ="Create invoice file";
    public static final String INVOICE_NULL_LOG                                ="Invoice object can't be null";

    private LogConstant(){

    }
}
