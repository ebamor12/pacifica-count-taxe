package com.pacifica.calculetaxe.common.constant;

public final class InvoiceConstant {

    public static final String TAXE_TOTAL                                       ="Montant des taxes :";
    public static final String TOTAL_PRIX                                       ="Total :";
    public static final String SPACE_STRING                                     =" ";
    public static final String START_INVOICE_STRING                             ="*";
    public static final String EURO_IN_INVOICE                                  ="€ :";
    public static final String EURO_IN_INVOICE_TOTAL                            ="€";
    public static final String A_IN_INVOICE                                     ="à";
    public static final String DELIMETER_ONE_LINE                               ="\n";
    public static final String DELIMETER_TWO_LINE                               ="\n\n";
    public static final String EMPTY_STRING                                     ="";

    private  InvoiceConstant(){

    }
}
