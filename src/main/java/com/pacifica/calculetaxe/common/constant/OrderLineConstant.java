package com.pacifica.calculetaxe.common.constant;

public final class OrderLineConstant {

    public static final String IMPORT_IN_LINE_ORDER                           ="import";
    public static final String A_IN_LINE_ORDER                                ="à";
    public static final String EURO_IN_LINE_ORDER                             ="€";
    public static final String EMPTY_STRING                                   ="";

    private OrderLineConstant(){

    }
}
