package com.pacifica.calculetaxe.common.constant;

public final class FileConstant {

    public static final String VALUE_PATTERN                                    ="\\d+";
    public static final String FILE_PATH                                        ="src/main/resources/";
    public static final String FIRST_COMMAND_FILE_PATH                          ="src/main/resources/FirstInputCommande.txt";
    public static final String THIRD_COMMAND_FILE_PATH                          ="src/main/resources/ThirdInputCommande.txt";
    public static final String SECOND_COMMAND_FILE_PATH                         ="src/main/resources/SecondInputCommande.txt";
    public static final String FILE_NAME                                        ="Facture_";
    public static final String DATE_PATTERN                                     ="yyyy-MM-dd HH-mm-ss";
    public static final String TXT_EXTENSION                                    =".txt";
    public static final String SEPARATION                                       ="_";
    private FileConstant(){

    }
}
