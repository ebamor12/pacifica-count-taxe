package com.pacifica.calculetaxe.common.utils;

import com.pacifica.calculetaxe.models.Invoice;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringJoiner;

import static com.pacifica.calculetaxe.common.constant.FileConstant.*;
import static com.pacifica.calculetaxe.common.constant.InvoiceConstant.EMPTY_STRING;
import static com.pacifica.calculetaxe.common.constant.LogConstant.INVOICE_NULL_LOG;

public final class FileUtil {

    public static File createNewFile(Invoice invoice) {
        if (invoice == null)
            throw new IllegalArgumentException(INVOICE_NULL_LOG);
        return new File(generateFileName(invoice.getTotaleProductsPrices().toString()));
    }

    private static String generateFileName(String totalPrices) {
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
        return new StringJoiner(EMPTY_STRING)
                .add(FILE_PATH)
                .add(FILE_NAME)
                .add(SEPARATION)
                .add(totalPrices)
                .add(dateFormat.format(date))
                .add(TXT_EXTENSION)
                .toString();
    }

    public static void loadDataInFile(File file, String text) throws IOException {
        FileWriter fileWriter = new FileWriter(file);
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.print(text);
        printWriter.close();
    }

    private FileUtil() {
    }
}
