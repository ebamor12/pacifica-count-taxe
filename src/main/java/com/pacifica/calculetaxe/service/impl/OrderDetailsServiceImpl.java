package com.pacifica.calculetaxe.service.impl;

import com.pacifica.calculetaxe.common.utils.ProductUtil;
import com.pacifica.calculetaxe.models.OrderDetails;
import com.pacifica.calculetaxe.service.OrderDetailsService;
import com.pacifica.calculetaxe.service.ProductService;

import static com.pacifica.calculetaxe.common.constant.OrderLineConstant.*;

public class OrderDetailsServiceImpl implements OrderDetailsService {

    @Override
    public OrderDetails createOrderDetailFromOrderLine(String orderLine, ProductService productService) {
        OrderDetails orderDetail = new OrderDetails();
        Integer productQuantity = ProductUtil.getProductQuantityFromOderLine(orderLine);
        orderDetail.setProduct(productService.createProductFromOrderLine(orderLine, productQuantity));
        orderDetail.setQuantity(productQuantity);
        orderDetail.setImportLibelle(this.getImportLibelle(orderLine, orderDetail));
        return orderDetail;
    }

    private String getImportLibelle(String orderLine, OrderDetails orderDetail) {
        return orderDetail.getProduct().isImported() ?
                orderLine.substring(orderLine.indexOf(IMPORT_IN_LINE_ORDER), (orderLine.indexOf(A_IN_LINE_ORDER))).trim() : EMPTY_STRING;
    }
}
