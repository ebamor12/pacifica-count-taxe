package com.pacifica.calculetaxe.service.impl;

import com.pacifica.calculetaxe.common.utils.FileUtil;
import com.pacifica.calculetaxe.models.Invoice;
import com.pacifica.calculetaxe.models.InvoiceDetails;
import com.pacifica.calculetaxe.models.Order;
import com.pacifica.calculetaxe.service.InvoiceDetailsService;
import com.pacifica.calculetaxe.service.InvoiceService;
import com.pacifica.calculetaxe.service.TaxeCalculatorService;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.StringJoiner;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static com.pacifica.calculetaxe.common.constant.InvoiceConstant.*;
import static com.pacifica.calculetaxe.common.constant.LogConstant.CREATE_INVOICE_FILE_LOG;
import static com.pacifica.calculetaxe.common.constant.LogConstant.INVOICE_NULL_LOG;

public class InvoiceServiceImpl implements InvoiceService {

    private static final Logger LOGGER = Logger.getLogger(InvoiceServiceImpl.class.getName());

    @Override
    public Invoice createInvoiceFromOrder(Order order, InvoiceDetailsService invoiceDetailsService, TaxeCalculatorService taxeCalculatorService) {
        Invoice invoice = new Invoice();
        List<InvoiceDetails> invoiceDetailsList = order.getOrderDetails()
                .stream()
                .map(orderDetails -> (invoiceDetailsService.createInvoiceDetail(orderDetails, taxeCalculatorService)))
                .collect(Collectors.toList());
        invoice.setInvoiceDetails(invoiceDetailsList);
        invoice.setTotalProductsTaxe(taxeCalculatorService.countTotalTax(order.getOrderDetails()));
        invoice.setTotaleProductsPrices(taxeCalculatorService.countTotalPriceTTC(order.getOrderDetails()));
        return invoice;
    }

    @Override
    public String createInvoiceFile(Invoice invoice) throws IOException {
        LOGGER.info(CREATE_INVOICE_FILE_LOG);
        File file = FileUtil.createNewFile(invoice);
        FileUtil.loadDataInFile(file, this.getInvoiceData(invoice));
        return file.getCanonicalPath();
    }

    private String getInvoiceData(Invoice invoice) {
        if (invoice == null)
            throw new IllegalArgumentException(INVOICE_NULL_LOG);
        return new StringJoiner(DELIMETER_TWO_LINE)
                .add(getInvoiceLinesText(invoice))
                .add(getTotalTaxAndPriceTTCLines(invoice))
                .toString();
    }

    private String getInvoiceLinesText(Invoice invoice) {
        if (invoice == null)
            throw new IllegalArgumentException(INVOICE_NULL_LOG);
        return invoice.getInvoiceDetails()
                .stream()
                .map(this::createInvoiceLineText)
                .collect(Collectors.joining(DELIMETER_ONE_LINE));
    }

    private String createInvoiceLineText(InvoiceDetails invoiceDetail) {
        String invoiceLinePart = new StringJoiner(SPACE_STRING)
                .add(START_INVOICE_STRING)
                .add(invoiceDetail.getQuantity().toString())
                .add(invoiceDetail.getProduct().getLibelle())
                .add(!invoiceDetail.getImportLibelle().isEmpty() ? invoiceDetail.getImportLibelle() : EMPTY_STRING)
                .toString()
                .trim();
        return new StringJoiner(SPACE_STRING)
                .add(invoiceLinePart)
                .add(A_IN_INVOICE)
                .add(getPriceWithCurrency(invoiceDetail.getProduct().getUnitPriceWithoutTaxe().toString(), EURO_IN_INVOICE))
                .add(getPriceWithCurrency(invoiceDetail.getPriceTTC().toString(), EURO_IN_INVOICE_TOTAL))
                .toString();
    }

    private String getTotalTaxAndPriceTTCLines(Invoice invoice) {
        if (invoice == null)
            throw new IllegalArgumentException(INVOICE_NULL_LOG);
        String totalTaxLine = getTotalAmount(TAXE_TOTAL, invoice.getTotalProductsTaxe().toString());
        String totalPriceLine = getTotalAmount(TOTAL_PRIX, invoice.getTotaleProductsPrices().toString());
        return new StringJoiner(DELIMETER_ONE_LINE)
                .add(getPriceWithCurrency(totalTaxLine, EURO_IN_INVOICE_TOTAL))
                .add(getPriceWithCurrency(totalPriceLine, EURO_IN_INVOICE_TOTAL))
                .toString();
    }

    private String getTotalAmount(String totalString, String totalValue) {
        return new StringJoiner(SPACE_STRING)
                .add(totalString)
                .add(totalValue)
                .toString();
    }

    private String getPriceWithCurrency(String priceText, String euroText) {
        return new StringJoiner(EMPTY_STRING)
                .add(priceText)
                .add(euroText)
                .toString();
    }
}
