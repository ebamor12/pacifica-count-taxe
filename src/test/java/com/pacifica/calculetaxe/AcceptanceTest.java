package com.pacifica.calculetaxe;

import com.pacifica.calculetaxe.common.utils.FileUtils;
import com.pacifica.calculetaxe.models.Invoice;
import com.pacifica.calculetaxe.service.*;
import com.pacifica.calculetaxe.service.impl.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static com.pacifica.calculetaxe.common.constant.CommonConstant.*;
import static com.pacifica.calculetaxe.common.constant.FileConstant.*;

public class AcceptanceTest {

    InvoiceService invoiceService;
    OrderService orderService;
    TaxeCalculatorService taxeCalculatorService;
    ProductService productService;
    OrderDetailsService orderDetailsService;
    InvoiceDetailsService invoiceDetailsService;


    @Before
    public void setUp() {
        invoiceService = new InvoiceServiceImpl();
        orderService = new OrderServiceImpl();
        taxeCalculatorService = new TaxeCalculatorServiceImpl();
        productService = new ProductServiceImpl();
        orderDetailsService = new OrderDetailsServiceImpl();
        invoiceDetailsService = new InvoiceDetailsServiceImpl();
    }

    @Test
    public void should_create_first_invoice_file() throws IOException {
        Invoice invoice = invoiceService.createInvoiceFromOrder(orderService.createOrderFromFile(FIRST_COMMAND_FILE_PATH, orderDetailsService, productService), invoiceDetailsService, taxeCalculatorService);
        Assert.assertTrue(FileUtils.isContentEqual(FIRST_EXPECTED_OUTPUT_FILE_PATH, invoiceService.createInvoiceFile(invoice)));
    }

    @Test
    public void should_create_second_invoice_file() throws IOException {
        Invoice invoice = invoiceService.createInvoiceFromOrder(orderService.createOrderFromFile(SECOND_COMMAND_FILE_PATH, orderDetailsService, productService), invoiceDetailsService, taxeCalculatorService);
        Assert.assertTrue(FileUtils.isContentEqual(SECOND_EXPECTED_OUTPUT_FILE_PATH, invoiceService.createInvoiceFile(invoice)));
    }

    @Test
    public void should_create_third_invoice_file() throws IOException {
        Invoice invoice = invoiceService.createInvoiceFromOrder(orderService.createOrderFromFile(THIRD_COMMAND_FILE_PATH, orderDetailsService, productService), invoiceDetailsService, taxeCalculatorService);
        Assert.assertTrue(FileUtils.isContentEqual(THIRD_EXPECTED_OUTPUT_FILE_PATH, invoiceService.createInvoiceFile(invoice)));
    }
}
