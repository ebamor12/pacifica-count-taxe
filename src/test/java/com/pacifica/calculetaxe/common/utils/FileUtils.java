package com.pacifica.calculetaxe.common.utils;

import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


public final class FileUtils {

    public static boolean isContentEqual(String outputFilePth, String expectedFilePath) throws IOException {
        Path path1 = Paths.get(expectedFilePath);
        Path path2 = Paths.get(outputFilePth);
        InputStream inputStream1 = new FileInputStream(path1.toFile());
        InputStream inputStream2 = new FileInputStream(path2.toFile());
        Files.writeString(path1, IOUtils.toString(inputStream1, StandardCharsets.UTF_8));
        Files.writeString(path2, IOUtils.toString(inputStream2, StandardCharsets.UTF_8));
        return IOUtils.contentEquals(inputStream1, inputStream2);
    }

    private FileUtils() {
    }
}
