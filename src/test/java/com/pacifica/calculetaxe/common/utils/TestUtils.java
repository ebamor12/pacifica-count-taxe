package com.pacifica.calculetaxe.common.utils;

import com.pacifica.calculetaxe.common.enumerations.ProductTypeEnum;
import com.pacifica.calculetaxe.models.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static com.pacifica.calculetaxe.common.constant.CommonConstant.*;

public class TestUtils {

    public static Invoice getInvoice() {
        Invoice invoice = new Invoice();
        List<InvoiceDetails> invoiceDetails = new ArrayList<>();
        invoiceDetails.add(getBookInvoiceDetail());
        invoiceDetails.add(getOtherInvoiceDetail());
        invoiceDetails.add(getChocolateInvoiceDetail());
        invoice.setInvoiceDetails(invoiceDetails);
        invoice.setTotalProductsTaxe(BigDecimal.valueOf(TOTAL_TAXE));
        invoice.setTotaleProductsPrices(BigDecimal.valueOf(TOTAL_PRICE_TTC));
        return invoice;
    }

    public static InvoiceDetails getChocolateInvoiceDetail() {
        InvoiceDetails InvoiceDetails = new InvoiceDetails();
        InvoiceDetails.setPriceTTC(BigDecimal.valueOf(CHOCOLATE_PRODUCT_PRICETTC));
        InvoiceDetails.setProduct(getChocolateProduct());
        InvoiceDetails.setQuantity(3);
        return InvoiceDetails;
    }

    public static InvoiceDetails getBookInvoiceDetail() {
        InvoiceDetails InvoiceDetails = new InvoiceDetails();
        InvoiceDetails.setPriceTTC(BigDecimal.valueOf(BOOK_PRODUCT_PRICETTC));
        InvoiceDetails.setProduct(getBookProduct());
        InvoiceDetails.setQuantity(BOOK_PRODUCT_QUNTITY);
        return InvoiceDetails;
    }

    public static InvoiceDetails getOtherInvoiceDetail() {
        InvoiceDetails InvoiceDetails = new InvoiceDetails();
        InvoiceDetails.setPriceTTC(BigDecimal.valueOf(OTHER_PRODUCT_PRICETTC));
        InvoiceDetails.setProduct(getOthersProduct());
        InvoiceDetails.setQuantity(OTHER_PRODUCT_QUNTITY);
        return InvoiceDetails;
    }

    public static Order getOrder() {
        Order order = new Order();
        List<OrderDetails> OrderDetails = new ArrayList<>();
        OrderDetails.add(getFirstOrderLine());
        OrderDetails.add(getSecondOrderLine());
        OrderDetails.add(getThirdOrderLine());


        order.setOrderDetails(OrderDetails);
        return order;
    }

    public static OrderDetails getFirstOrderLine() {
        OrderDetails ligneCommandes = new OrderDetails();
        ligneCommandes.setProduct(getBookProduct());
        ligneCommandes.setQuantity(BOOK_PRODUCT_QUNTITY);
        ligneCommandes.setImportLibelle(EMPTY_STRING);
        return ligneCommandes;
    }

    public static OrderDetails getSecondOrderLine() {
        OrderDetails ligneCommandes = new OrderDetails();
        ligneCommandes.setProduct(getOthersProduct());
        ligneCommandes.setQuantity(OTHER_PRODUCT_QUNTITY);
        ligneCommandes.setImportLibelle(EMPTY_STRING);
        return ligneCommandes;
    }

    public static OrderDetails getThirdOrderLine() {
        OrderDetails ligneCommandes = new OrderDetails();
        ligneCommandes.setProduct(getChocolateProduct());
        ligneCommandes.setQuantity(PRODUCT_QUNTITY);
        ligneCommandes.setImportLibelle(EMPTY_STRING);
        return ligneCommandes;
    }

    public static Product getChocolateProduct() {
        Product product = new Product();
        product.setImported(false);
        product.setLibelle(CHOCOLATE_PROUCT_LIBELLE);
        product.setUnitPriceWithoutTaxe(BigDecimal.valueOf(UNIT_PRICE_CHOCOLATE_PRODUCT));
        product.setProductType(ProductTypeEnum.FOOD);
        return product;
    }

    public static Product getBookProduct() {
        Product product = new Product();
        product.setImported(false);
        product.setLibelle(BOOK_PROUCT_LIBELLE);
        product.setUnitPriceWithoutTaxe(BigDecimal.valueOf(UNIT_PRICE_BOOK_PRODUCT));
        product.setProductType(ProductTypeEnum.BOOK);
        return product;
    }

    public static Product getOthersProduct() {
        Product product = new Product();
        product.setImported(false);
        product.setLibelle(OTHER_CD_PROUCT_LIBELLE);
        product.setUnitPriceWithoutTaxe(BigDecimal.valueOf(UNIT_PRICE_OTHER_PRODUCT));
        product.setProductType(ProductTypeEnum.OTHERS);
        return product;
    }

    public static Product getPillProduct() {
        Product product = new Product();
        product.setImported(false);
        product.setLibelle(PILL_PROUCT_LIBELLE);
        product.setUnitPriceWithoutTaxe(BigDecimal.valueOf(UNIT_PRICE_PILL_PRODUCT));
        product.setProductType(ProductTypeEnum.MEDICATION);
        return product;
    }

    public static Product getOtherProduct() {
        Product product = new Product();
        product.setImported(false);
        product.setLibelle(OHER_PROUCT_LIBELLE);
        product.setUnitPriceWithoutTaxe(BigDecimal.valueOf(UNIT_PRICE_PILL_PRODUCT));
        product.setProductType(ProductTypeEnum.OTHERS);
        return product;
    }
}
