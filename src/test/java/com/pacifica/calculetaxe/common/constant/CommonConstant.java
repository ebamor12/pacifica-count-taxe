package com.pacifica.calculetaxe.common.constant;

public final class CommonConstant {
    public static final String OHER_PROUCT_LIBELLE                          ="casques";
    public static final String PILL_PROUCT_LIBELLE                          ="boîtes de pilules contre la migraine";
    public static final String OTHER_CD_PROUCT_LIBELLE                      ="CD musical";
    public static final String BOOK_PROUCT_LIBELLE                          ="livres";
    public static final String CHOCOLATE_PROUCT_LIBELLE                     ="barres de chocolat";
    public static final String FIRST_FILE_PATH                              ="src/main/resources/FirstInputCommande.txt";
    public static final String FIRST_EXPECTED_OUTPUT_FILE_PATH              ="src/test/resources/FirstOutputExpected.txt";
    public static final String SECOND_EXPECTED_OUTPUT_FILE_PATH             ="src/test/resources/SecondOutputExpected.txt";
    public static final String THIRD_EXPECTED_OUTPUT_FILE_PATH              ="src/test/resources/ThirdOutputExpected.txt";
    public static final String EMPTY_STRING                                 ="";
    public static final Integer PRODUCT_QUNTITY                             =3;
    public static final Integer OTHER_PRODUCT_QUNTITY                       =1;
    public static final Integer BOOK_PRODUCT_QUNTITY                        =2;
    public static final Integer OTHER_PRODUCT_PRICETTC                      =18;
    public static final Double UNIT_PRICE_PILL_PRODUCT                      =9.75;
    public static final Double UNIT_PRICE_OTHER_PRODUCT                     =14.99;
    public static final Double UNIT_PRICE_BOOK_PRODUCT                      =12.49;
    public static final Double UNIT_PRICE_CHOCOLATE_PRODUCT                 =0.85;
    public static final Double BOOK_PRODUCT_PRICETTC                        =27.5;
    public static final Double CHOCOLATE_PRODUCT_PRICETTC                   =2.55;
    public static final Double TOTAL_TAXE                                   =5.53;
    public static final Double TOTAL_PRICE_TTC                              =48.05;

    private CommonConstant() {
    }
}
