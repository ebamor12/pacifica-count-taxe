package com.pacifica.calculetaxe.common.constant;

public final class OrderLinesConstant {
    public static final String BOOK_PRODUCT_ORDERLINE                       ="* 2 livres à 12.49€";
    public static final String CD_PRODUCT_ORDERLINE                         ="* 1 CD musical à 14.99€";
    public static final String CHOCOLATE_PRODUCT_ORDERLINE                  ="* 3 barres de chocolat à 0.85€";
    public static final String PILL_PRODUCT_ORDERLINE                       ="* 3 boîtes de pilules contre la migraine à 9.75€";
    public static final String CASQUE_PRODUCT_ORDERLINE                     ="* 3 casques à 9.75€";

    private OrderLinesConstant() {
    }
}
