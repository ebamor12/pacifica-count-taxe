package com.pacifica.calculetaxe.services.impl;

import com.pacifica.calculetaxe.common.utils.TestUtils;
import com.pacifica.calculetaxe.models.OrderDetails;
import com.pacifica.calculetaxe.service.TaxeCalculatorService;
import com.pacifica.calculetaxe.service.impl.TaxeCalculatorServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

import static com.pacifica.calculetaxe.common.constant.CommonConstant.*;

public class TaxeCalculatorServiceImplTest {

    TaxeCalculatorService taxeCalculatorService;

    @Before
    public void setUp() {
        taxeCalculatorService = new TaxeCalculatorServiceImpl();
    }

    @Test
    public void should_calculate_product_priceTTC() {
        OrderDetails orderDetails = TestUtils.getFirstOrderLine();
        BigDecimal productPriceTTC = taxeCalculatorService.countProductPriceTTC(orderDetails);
        BigDecimal expectedProductPriceTTC = BigDecimal.valueOf(BOOK_PRODUCT_PRICETTC);
        Assert.assertEquals(expectedProductPriceTTC, productPriceTTC);

    }

    @Test
    public void should_calculate_product_totale_priceTTC() {
        List<OrderDetails> orderDetailsList = TestUtils.getOrder().getOrderDetails();
        BigDecimal totaleProductPriceTTC = taxeCalculatorService.countTotalPriceTTC(orderDetailsList);
        BigDecimal expectedProductPriceTTC = BigDecimal.valueOf(TOTAL_PRICE_TTC);
        Assert.assertEquals(expectedProductPriceTTC, totaleProductPriceTTC);
    }

    @Test
    public void should_calculate_product_totale_taxe() {
        List<OrderDetails> orderDetailsList = TestUtils.getOrder().getOrderDetails();
        BigDecimal totaleTaxe = taxeCalculatorService.countTotalTax(orderDetailsList);
        BigDecimal expectedTotaleTaxe = BigDecimal.valueOf(TOTAL_TAXE);
        Assert.assertEquals(expectedTotaleTaxe, totaleTaxe);
    }
}
