package com.pacifica.calculetaxe.services.impl;

import com.pacifica.calculetaxe.common.utils.TestUtils;
import com.pacifica.calculetaxe.models.Product;
import com.pacifica.calculetaxe.service.impl.ProductServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static com.pacifica.calculetaxe.common.constant.CommonConstant.*;
import static com.pacifica.calculetaxe.common.constant.OrderLinesConstant.*;


public class ProductServiceImplTest {

    ProductServiceImpl productServiceImpl;

    @Before
    public void setUp() {
        productServiceImpl = new ProductServiceImpl();
    }

    @Test
    public void should_create_book_product_from_orderline() {
        Product expectedProduct = TestUtils.getBookProduct();
        Product product = productServiceImpl.createProductFromOrderLine(BOOK_PRODUCT_ORDERLINE, BOOK_PRODUCT_QUNTITY);
        Assert.assertEquals(expectedProduct, product);
    }

    @Test
    public void should_create_other_product() {
        Product expectedProduct = TestUtils.getOthersProduct();
        Product product = productServiceImpl.createProductFromOrderLine(CD_PRODUCT_ORDERLINE, OTHER_PRODUCT_QUNTITY);
        Assert.assertEquals(expectedProduct, product);
    }

    @Test
    public void should_create_food_product() {
        Product expectedProduct = TestUtils.getChocolateProduct();
        Product product = productServiceImpl.createProductFromOrderLine(CHOCOLATE_PRODUCT_ORDERLINE, PRODUCT_QUNTITY);
        Assert.assertEquals(expectedProduct, product);
    }

    @Test
    public void should_create_medication_product() {
        Product expectedProduct = TestUtils.getPillProduct();
        Product product = productServiceImpl.createProductFromOrderLine(PILL_PRODUCT_ORDERLINE, PRODUCT_QUNTITY);
        Assert.assertEquals(expectedProduct, product);
    }

    @Test
    public void should_create_other_product_when_product_name_dosent_exist() {
        Product expectedProduct = TestUtils.getOtherProduct();
        Product product = productServiceImpl.createProductFromOrderLine(CASQUE_PRODUCT_ORDERLINE, PRODUCT_QUNTITY);
        Assert.assertEquals(expectedProduct, product);
    }
}
