package com.pacifica.calculetaxe.services.impl;

import com.pacifica.calculetaxe.common.utils.TestUtils;
import com.pacifica.calculetaxe.models.InvoiceDetails;
import com.pacifica.calculetaxe.models.OrderDetails;
import com.pacifica.calculetaxe.service.InvoiceDetailsService;
import com.pacifica.calculetaxe.service.TaxeCalculatorService;
import com.pacifica.calculetaxe.service.impl.InvoiceDetailsServiceImpl;
import com.pacifica.calculetaxe.service.impl.TaxeCalculatorServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class InvoiceDetailsServiceImplTest {

    InvoiceDetailsService invoiceDetailsService;

    @Before
    public void setUp() {
        invoiceDetailsService = new InvoiceDetailsServiceImpl();
    }

    @Test
    public void should_create_invoiceDetail_from_orderDetails() {
        OrderDetails orderDetail = TestUtils.getSecondOrderLine();
        TaxeCalculatorService taxeCalculatorService = new TaxeCalculatorServiceImpl();
        InvoiceDetails expectedInvoiceDetails = TestUtils.getOtherInvoiceDetail();
        InvoiceDetails invoiceDetails = invoiceDetailsService.createInvoiceDetail(orderDetail, taxeCalculatorService);
        Assert.assertEquals(expectedInvoiceDetails, invoiceDetails);
    }
}
